// server.js
let express = require('express');
let path = require('path');
let serveStatic = require('serve-static');
let port = process.env.PORT || 5000;

let app = express();
app.use(express.static('dist'));

app.get('*', (request, response) => {
	response.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.listen(port);

console.log('server started '+ port);