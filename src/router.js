/**
 * Router file to set routes.
 */
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ShowAllTask from './views/ShowAllTask.vue'
import AddTask from './views/AddTask.vue'
import ShowTask from './views/ShowTask.vue'
import Signin from './views/Signin.vue'

import store from '@/store';

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/showAll',
      name: 'showAllTask',
      component: ShowAllTask
    },
    {
      path: '/addTask',
      name: 'addTask',
      component: AddTask
    },
    {
      path: '/showTask',
      name: 'showTask',
      props: true,
      component: ShowTask
    },
    {
      path: '/login',
      name: 'Zaloguj',
      component: Signin,
      meta: {
        dontRequiredAuth: true,
      },
    },
  ]
});

/**
 * Check every route and decide is available
 * @param {object} to router next address
 * @param {object} from router previous address
 * @param {function} next route to next address
 */
router.beforeEach((to, from, next) => {
  const dontRequiredAuth = to.matched.some(record => record.meta.dontRequiredAuth);

  if (dontRequiredAuth) {
    next();
  } else if (store.getters.user) {
    next();
  } else {
    next('/login');
  }
});

export default router;