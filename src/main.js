/**
 * Main file which loading all modules
 */
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'

import './filters/filters'

import AlertComponent from './components/Alert.vue'

Vue.component('app-alert', AlertComponent)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
