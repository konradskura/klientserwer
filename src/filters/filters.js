import Vue from 'vue'

/**
 * Filter for filter order status
 * @param {string} val order status
 * @returns {Object} mapped status
 */
Vue.filter('status', function (val) {
    const statuses = [
      {
        id: 1,
        name: 'Zgłoszony'
      },
      {
        id: 2,
        name: 'W przetwarzaniu'
      },
      {
        id: 3,
        name: 'Przekierowany'
      },
      {
        id: 4,
        name: 'Rozwiązany'
      },
      {
        id: 5,
        name: 'Zamknięty'
      }
    ]
    let status = statuses.find(status => val == status.id)

    return status ? status.name : 'Nieznany'
})

/**
 * Filter for display date in readable format
 * @param {int} val date miliseconds
 * @returns {string} readable date
 */
Vue.filter('fullDate', (val) => {
  const date = new Date(parseInt(val));

  return date.toLocaleDateString(['pl-Pl'], {
    month: '2-digit',
    day: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit' });
});