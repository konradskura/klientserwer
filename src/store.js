/**
 * Vuex store
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import sha256 from 'js-sha256'

Vue.use(Vuex)

const apiUrl = 'http://51.91.58.139:9001'
const testToken = 'QSCDXRJMOWFRXSJYBLDB'

export default new Vuex.Store({
  state: {
    loading: false,
    tasks: [],
    users: [],
    user: null,
    error: null,
    statuses: [
      {
        id: 1,
        name: 'Zgłoszony'
      },
      {
        id: 2,
        name: 'W przetwarzaniu'
      },
      {
        id: 3,
        name: 'Przekierowany'
      },
      {
        id: 4,
        name: 'Rozwiązany'
      },
      {
        id: 5,
        name: 'Zamknięty'
      }
    ]
  },
  mutations: {
    /**
     * Set loader state.
     * @param {Object} state - state
     * @param {boolean} payload - data
     */
    setLoading(state, payload) {
      state.loading = payload
    },
    /**
     * Set tasks in store.
     * @param {Object} state - state
     * @param {Object} tasks - tasks to save
     */
    fetchTasks(state, tasks) {
      // convert JSON to collection
      let result = []
      for (let task in tasks) {
        result.push(tasks[task])
      }

      state.tasks = result
    },
    /**
     * Update task
     * @param {Object} state - state
     * @param {Object} payload - task data
     */
    updateTask(state, payload) {
      const tasks = state.tasks

      tasks.find(task => task._id.$oid === payload.id).history.push(payload)

      state.tasks = tasks
    },
    /**
     * Set users data
     * @param {Object} state - state
     * @param {Object} payload - user data
     */
    fetchUsers(state, users) {
      let result = []
      for (let user in users) {
        result.push(users[user])
      }

      state.users = result
    },
    /**
     * Set user profile
     * @param {Object} state - state
     * @param {Object} payload - user data
     */
    fetchUser(state, user) {
      state.user = user
    },
    /**
     * Logout user
     * @param {Object} state - state
     */
    logout(state) {
      state.user = null
    },
    /**
     * Set error message
     * @param {Object} state - state
     * @param {string} payload - error message
     */
    setError(state, payload) {
      state.error = payload;
    },
    /**
     * Clear error message
     * @param {Object} state - state 
     */
    clearError(state) {
      state.error = null;
    },
  },
  actions: {
    /**
     * Get tasks from server
     * @param {Object} param0 - store methods
     */
    fetchTasks({ commit, getters }) {
      commit('setLoading', true)

      axios({
        method: 'GET',
        headers: { 
          'x-token': `${getters.user.token}`,
          'content-type': 'application/x-www-form-urlencoded',
        },
        timeout: 10000,
        url: `${apiUrl}/downloadAllTasks`
      })
      .then((response) => {
        commit('clearError')
        commit("fetchTasks", response.body || response.data)
        // TODO remove response.data
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Nie udało się pobrać zawartości. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Get users from server
     * @param {Object} param0 - store methods
     */
    fetchUsers({ commit, getters }) {
      commit('setLoading', true)
      
      axios({
        method: 'GET',
        headers: { 
          'content-type': 'application/x-www-form-urlencoded',
          'x-token': `${getters.user ? getters.user.token : testToken}`
        },
        timeout: 10000,
        url: `${apiUrl}/getAllUsers`

      })
      .then((response) => {
        commit('clearError')
        commit("fetchUsers", response.body || response.data)
        // TODO remove response.data
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Nie udało się pobrać zawartości użytkowników. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Add task to database
     * @param {Object} param0 - store methods
     * @param {Object} payload - task data
     */
    addTask({ commit, getters }, payload) {
      const order = {
        ...payload
      }

      commit('setLoading', true)

      axios({
        method: 'GET',
        headers: { 
          'content-type': 'application/x-www-form-urlencoded',
          'x-token': `${getters.user ? getters.user.token : testToken}`
        },
        // data: qs.stringify(order),
        params: order,
        timeout: 10000,
        url: `${apiUrl}/createTask`
      })
      .then(() => {
        commit('clearError')
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Nie udało się dodać zgłoszenia. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Save new task data in database
     * @param {Object} param0 - store methods
     * @param {Object} payload - task data
     */
    editTask({ commit, getters }, payload) {
      const order = {
        ...payload
      }

      commit('setLoading', true)

      axios({
        method: 'GET',
        headers: { 
          'content-type': 'application/x-www-form-urlencoded',
          'x-token': `${getters.user ? getters.user.token : testToken}`
        },
        params: order,
        timeout: 10000,
        url: `${apiUrl}/changeTaskStatus`
      })
      .then(() => {
        commit('clearError')
        commit('updateTask', order)
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Nie udało się modyfikować zawartości. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Sign in user
     * @param {Object} param0 - store methods
     * @param {Object} payload - user data
     */
    signIn({ commit }, payload) {
      const logData = {
        username: payload.username,
        password: sha256(payload.password)
      }
      commit('setLoading', true)

      axios({
        method: 'GET',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        params: logData,
        timeout: 10000,
        url: `${apiUrl}/login`
      })
      .then((response) => {
        commit("fetchUser", response.data)
        commit('clearError')
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Błędne dane. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Log out
     * @param {Object} param0 - store methods
     */
    logout({ commit, getters }) {
      
      axios({
        method: 'GET',
        headers: { 
          'content-type': 'application/x-www-form-urlencoded',
          'x-token': `${getters.user ? getters.user.token : testToken}`
        },
        timeout: 10000,
        url: `${apiUrl}/logout`
      })
      .then(() => {
        commit('clearError');
        commit('logout')
        commit('setLoading', false)
      })
      .catch(() => {
        commit('setError', 'Nie udało się wylodować. Spórbuj jeszcze raz.')
        commit('setLoading', false)
      })
    },
    /**
     * Clear error state
     * @param {object} param0 - store methods
     */
    clearError({ commit }) {
      commit('clearError')
    }
  },
  getters: {
    /**
     * Get tasks
     * @param {object} state - state
     * @returns {object} - tasks
     */
    tasks(state) {
      return state.tasks
    },
    /**
     * Get task
     * @param {object} state - state
     * @returns {object} - task
     */
    task(state) {
      return taskId => state.tasks.find(task => taskId === task._id.$oid)
    },
    /**
     * Get loader
     * @param {object} state - state
     * @returns {boolean} - loader stataus
     */
    loading(state) {
      return state.loading
    },
    /**
     * Get users
     * @param {object} state - state
     * @returns {array} - users
     */
    users(state) {
      return state.users
    },
    /**
     * Get statuses
     * @param {object} state - state
     * @returns {array} - collection of statuses
     */
    statuses(state) {
      let statuses = state.statuses

      if (state.user.role !== '2') {
        statuses = statuses.filter(status => status.name !== 'Zamknięty')
      }

      return statuses
    },
    /**
     * Get user profile
     * @param {object} state - state
     * @returns {object} - user profile
     */
    user(state) {
      return state.user;
    },
    /**
     * Get error message
     * @param {object} state - state
     * @returns {string} - error message
     */
    error(state) {
      return state.error;
    },
  }
})
